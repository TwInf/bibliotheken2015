/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.cards;

import java.util.Comparator;
import java.util.Objects;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 * Eine Spielkarte.
 * Veraltet!
 * Diese ist die erste Entwurf, unter der package
 * de.twyman.Spielkarten ist eine verbesserte
 * Spielkarte Klasse entwickelt.
 * Die 'Werte' sind anderes und verschiedene 
 * Bildemenge sind erlaubt.
 * @author D. Twyman
 */
public class SpielKarte implements Comparable<SpielKarte> 
                                  ,Comparator<SpielKarte> {
    private final String[] Buchstabe1 = {"2","3","4",
                                         "5","6",
                                         "7","8","9",
                                         "t", //ten
                                         "j", //jack-Bube
                                         "q", //queen-Königin
                                         "k", //king-König
                                         "a"  //ass
                                         };
    private boolean bVorwarts = false;
    private int iKartenzahl; //2 bis 10, Bube, Königin, König, Ass
                             //2-9, ten, Jack, Queen, King, Ace
    /**
     * Die mögliche Farben von Spielkarten
     */
    public enum Spielkartenfarbe { 
                    PIK, HERZ, KARO, KREUZ, JOKER };
    private Spielkartenfarbe Farbe;

    /**
     * SpielKarte erstellen, 
     * zufällig aus alle mögliche Karten.
     * Hier eine Deck von Poker-Karten
     * ergibt die Wahrscheinlichkeiten
     * von Einzelnkarten.
     * Der Karte ist immer gedeckt.
     */
    public SpielKarte(){
        Random rnd = new Random();
        // Zufällige Farbe für die Spielkarte
        switch (rnd.nextInt(3)) {
            case 0:
                Farbe = Spielkartenfarbe.PIK;
                break;
            case 1:
                Farbe = Spielkartenfarbe.HERZ;
                break;
            case 2:
                Farbe = Spielkartenfarbe.KARO;
                break;
            case 3:
                Farbe = Spielkartenfarbe.KREUZ;       
                break;
        }
        // Zufällige Wert zwischen 1 und 13, inklusiv.
        iKartenzahl = rnd.nextInt(12)+1; 
    }
    
    /**
     * Bestimmte SpielKarte erstellen.
     * Hier eine Deck von Poker-Karten
     * ergibt die Wahrscheinlichkeiten
     * von Einzelnkarten.
     * Der Karte ist gedeckt.
     * @param fFarbe
     * @param iZahl
     */
    public SpielKarte(Spielkartenfarbe fFarbe
                     ,int iZahl){
        Farbe = fFarbe;
        if (fFarbe != Spielkartenfarbe.JOKER) {
            iKartenzahl = iZahl; 
        }
        else {
            iKartenzahl = 2; 
        }
    }
    
    /**
     * Implementiert die java.lang.Comparable Methode.
     * -1 diese Objekt ist kleiner als b
     * 0 diese Objekt ist gleich b
     * 1 diese Objekt ist großer als b
     * @param b
     */
    @Override
    public int compareTo(SpielKarte b) {
        int iMeinFarbe = FarbeAlsInt(Farbe);
        int iFarbeB    = FarbeAlsInt(b.getSpielfarbe());
        if(iMeinFarbe>iFarbeB) {
            return 1; // Mein Farbe ist großer
        }
        else if (iMeinFarbe<iFarbeB) {
            return -1; //Mein Farbe ist kleiner
        }
        else {  // die Farben sind gleich
            if (iKartenzahl>b.getWert()) {
                return 1;  // Mein Wert ist großer
            }
            else if (iKartenzahl<b.getWert()) {
                return -1;  // Mein Wert ist kleiner
            }
            else {  // Die Farben und Werte sind beide Gleich
                return 0;
            }
        }
    }
    
    /**
     * Implementiert die java.lang.Comparator Methode.
     * -1: A ist kleiner als B
     *  0: A ist gleich B
     *  1: A ist großer als B
     * @param A
     * @param B
     */
    @Override
    public int compare(SpielKarte A, SpielKarte B) {
        int iFarbeA = FarbeAlsInt(A.getSpielfarbe());
        int iFarbeB = FarbeAlsInt(B.getSpielfarbe());
        if(iFarbeA>iFarbeB) {
            return 1; // Farbe A ist großer
        }
        else if (iFarbeA<iFarbeB) {
            return -1; // Farbe A ist kleiner
        }
        else {  // die Farben sind gleich
            if (A.getWert()>B.getWert()) {
                return 1;  // Wert A ist großer
            }
            else if (A.getWert()<B.getWert()) {
                return -1;  // Wert A ist kleiner
            }
            else {  // Die Farben und Werte sind beide Gleich
                return 0;
            }
        }
    }
  
    /**
     * Die Karte wird gedreht.
     */
    public void drehen(){
        bVorwarts = !bVorwarts;
    }
    
    /**
     * Die Karte wird gedeckt.
     */
    public void decken() {
        bVorwarts = false;
    }
            
    /**
     * Die Karte wird aufgedeckt.
     */
    public void aufdecken() {
        bVorwarts = true;
    }
    
/*
    Eine Werkzeug-funktion, 
    um die Vergleich zu vereinfachen
    */
    final private int FarbeAlsInt(Spielkartenfarbe fFarbe){
        int retVal;
        switch (fFarbe) {
            case PIK:
                retVal=0;  
                break;
            case HERZ:
                retVal=1;
                break;
            case KARO:
                retVal=2;
                break;
            case KREUZ:
                retVal=3;
                break;   
            case JOKER:
            default:
                retVal=4;
                break;
        }
        
        return retVal;
    }
    
    /*
    Eine Werkzeug-funktion, 
    um die Dateiname zu erzeugen
    */
    private String FarbeAlsBuchstabe(){
        String retVal = "";
        switch (Farbe) {
            case PIK:
                retVal="s";  //Spades
                break;
            case HERZ:
                retVal="h";  //hearts
                break;
            case KARO:
                retVal="d";  // diamonds
                break;
            case KREUZ:
                retVal="c";  // clubs
                break;       
        }
        
        return retVal;
    }
            
    /**
     * Ein Bild von diese Karte als icon.
     * @return ImageIcon
     */
    public ImageIcon getIcon(){
        String Filename;
        if (!bVorwarts) {
            Filename = "/de/twyman/cards/b.gif";
        } else if (Spielkartenfarbe.JOKER == Farbe) {
            Filename = "/de/twyman/cards/j.gif";
        } else {
            Filename = "/de/twyman/cards/"
                        +Buchstabe1[iKartenzahl-1]
                        +this.FarbeAlsBuchstabe()
                        +".gif";
        }
        ImageIcon icon;
        icon = new ImageIcon(
                getClass().getResource(Filename));
        return icon;
    } 
    
    /**
     * Gibt die Spielkartenfarbe von diese Karte zurück.
     * @return Farbe
     */
    public Spielkartenfarbe getSpielfarbe() {
        return Farbe;
    }
            
    /**
     * Gibt die Wert von diese Karte zurück.
     * @return int
     */
    public int getWert() {
        return iKartenzahl;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.iKartenzahl;
        hash = 41 * hash + Objects.hashCode(this.Farbe);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SpielKarte other = (SpielKarte) obj;
        if (this.Farbe != other.Farbe) {
            return false;
        }
        if (Spielkartenfarbe.JOKER == this.Farbe)
            return true;
        if (this.iKartenzahl != other.iKartenzahl) {
            return false;
        }
        return true;
    }
    
    
}
