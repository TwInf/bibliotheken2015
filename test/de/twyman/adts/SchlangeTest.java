/*
 * Copyright (C) 2014 dominic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;

import java.util.NoSuchElementException;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dominic
 */
public class SchlangeTest {
    
    Random rnd;
    
    public SchlangeTest() {
        rnd = new Random();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of istLeer method, of class Schlange.
     */
    @Test
    public void testIstLeer() {
        Schlange<String> instance = new Schlange<>();
        assertTrue(instance.istLeer());
        instance.anhaengen("a");
        assertFalse(instance.istLeer());
    }

    /**
     * Test of inhaltGeben method, of class Schlange.
     */
    @Test
    public void testAnhaengenInhaltGeben() {
        Schlange<Integer> instance = new Schlange<>();
        instance.anhaengen(23);
        instance.anhaengen(32);
        assertEquals((Integer)23,instance.inhaltGeben());
        instance.anhaengen(43);
        assertEquals((Integer)23,instance.inhaltGeben());
        instance.entnehmen();
        assertEquals((Integer)32,instance.inhaltGeben());
    }


    
    /**
     * Test of entnehmen method, of class Schlange.
     */
    @Test
    public void testEntnehmen() {
        Schlange<Integer> instance = new Schlange<>();

        instance.anhaengen(23);
        instance.anhaengen(32);
        assertEquals((Integer)23,instance.inhaltGeben());
        Integer RetWert;
        RetWert = instance.entnehmen();
        assertEquals((Integer)23,RetWert);
        assertEquals((Integer)32,instance.inhaltGeben());
        RetWert = instance.entnehmen();
        assertEquals((Integer)32,RetWert);

        instance.anhaengen(43);
        assertEquals((Integer)43,instance.inhaltGeben());
        RetWert = instance.entnehmen();
        assertEquals((Integer)43,RetWert);
        assertEquals(null,instance.inhaltGeben());
    }

    /**
     * Test of entnehmen method, of class Schlange.
     */
    @Test(expected = NoSuchElementException.class)
    public void testEntnehmenLeer() {
        Schlange<Integer> instance = new Schlange<>();

        instance.entnehmen();
    }    
    

    /**
     * Test of equals method, of class Schlange.
     */
    @Test
    public void testHashCodeEquals() {
        // die sind nicht gleich
        Schlange<Integer> a = new Schlange<Integer>();
        Schlange<Integer> b = new Schlange<Integer>();
        
        // die drei sind alle gleich
        Schlange<Integer> x = new Schlange<Integer>();
        Schlange<Integer> y = new Schlange<Integer>();
        Schlange<Integer> z = new Schlange<Integer>();

        // alle haben zwischen 10 und 20 Zahlen
        int j= rnd.nextInt(10)+10;
        for(int i=0; i<j; i++) {
            // die Zahlen sind zwischen -500 und 500
            Integer k = rnd.nextInt(1000)-500;
            x.anhaengen(k);
            y.anhaengen(k);
            z.anhaengen(k);

            // ab und zu ist bei a ein zahl anderes.
            // aber das dritte ist immer anderes
            a.anhaengen(k+rnd.nextInt(2)+((i==2)?1:0));
            // b ist gleich
            b.anhaengen(k);
        }
        // b hat aber noch ein element
        b.anhaengen(23);

        // gleichheits regelen beachten
        assertTrue(   x.equals(y) && y.equals(x));
        assertTrue(   y.equals(z) && z.equals(y));
        assertTrue(   x.equals(z) && z.equals(x));
        assertTrue(   x.equals(x));
        
        assertFalse(  a.equals(x)  );
        assertFalse(  x.equals(a)  );
        assertFalse(  b.equals(x)  );
        assertFalse(  x.equals(b)  );
        assertFalse(  x.equals(rnd));
        
        // hashcode regelen beachten
        assertTrue(  x.hashCode() == y.hashCode());        
        assertTrue(  y.hashCode() == z.hashCode());        
        assertTrue(  z.hashCode() == x.hashCode());        
        
        //könnte sein, sollte aber wirklich nicht
        assertFalse( a.hashCode() == x.hashCode());        
        assertFalse( b.hashCode() == x.hashCode());        
    }
    
}
