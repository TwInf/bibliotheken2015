/*
 * Copyright (C) 2014 dominic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;

import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dominic
 */
public class ListeTest {
    
    Random rnd;
        
    public ListeTest() {
        rnd = new Random();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of hashCode method, of class Liste.
     */
    @Test
    public void testHashCodeEquals() {
        // wir generieren Drei identische und ein nicht identische
        // Versionen von die Listen und vergleichen die.
        
        // die sind nicht gleich
        Liste<Integer> a = new Liste<Integer>();
        Liste<Integer> b = new Liste<Integer>();
        
        // die drei sind alle gleich
        Liste<Integer> x = new Liste<Integer>();
        Liste<Integer> y = new Liste<Integer>();
        Liste<Integer> z = new Liste<Integer>();

        // alle haben zwischen 10 und 20 Zahlen
        int j= rnd.nextInt(10)+10;
        for(int i=0; i<j; i++) {
            // die Zahlen sind zwischen -500 und 500
            Integer k = rnd.nextInt(1000)-500;
            x.einfuegen(k);
            y.einfuegen(k);
            z.einfuegen(k);

            // ab und zu ist bei a ein zahl anderes.
            // aber das dritte ist immer anderes
            a.einfuegen(k+rnd.nextInt(2)+((i==2)?1:0));
            // b ist gleich
            b.einfuegen(k);
        }
        // b hat aber noch ein element
        b.einfuegen(23);

        // gleichheits regelen beachten
        assertTrue(   x.equals(y) && y.equals(x));
        assertTrue(   y.equals(z) && z.equals(y));
        assertTrue(   x.equals(z) && z.equals(x));
        assertTrue(   x.equals(x));
        
        assertFalse(  a.equals(x)  );
        assertFalse(  x.equals(a)  );
        assertFalse(  b.equals(x)  );
        assertFalse(  x.equals(b)  );
        assertFalse(  x.equals(rnd));
        
        // hashcode regelen beachten
        assertTrue(  x.hashCode() == y.hashCode());        
        assertTrue(  y.hashCode() == z.hashCode());        
        assertTrue(  z.hashCode() == x.hashCode());        
        
        //könnte sein, sollte aber wirklich nicht
        assertFalse( a.hashCode() == x.hashCode());        
        assertFalse( b.hashCode() == x.hashCode());
    }
    
    /**
     * Test of istLeer method, of class Liste.
     */
    @Test
    public void testIstLeer() {
        Liste<Integer> instance = new Liste<>();
        assertTrue(instance.istLeer());
        instance.einfuegen(23);
        assertFalse(instance.istLeer());
    }

    /**
     * Test of inhaltGeben method, of class Liste.
     */
    @Test
    public void testInhaltGebenEinfuegen() {
        Liste<Integer> instance = new Liste<>();

        assertEquals(null,instance.inhaltGeben());
        instance.einfuegen(23);
        instance.einfuegen(32);
        assertEquals((Integer)32,instance.inhaltGeben());
        assertEquals((Integer)32,instance.inhaltGeben());
        instance.einfuegen(43);
        assertEquals((Integer)43,instance.inhaltGeben());
        instance.positionSetzen(0);
        assertEquals((Integer)23,instance.inhaltGeben());
    }

    /**
     * Test of einfuegen method, of class Liste.
     */
    @Test
    public void testEinfuegen_Integer_GenericType() {
        Liste<String> instance = new Liste<>();

        instance.einfuegen("a");
        instance.einfuegen("c");
        instance.einfuegen(1, "b");
        assertEquals("b",instance.inhaltGeben());
        instance.positionSetzen(0);
        assertEquals("a",instance.inhaltGeben());
        instance.positionSetzen(1);
        assertEquals("b",instance.inhaltGeben());
        instance.positionSetzen(2);
        assertEquals("c",instance.inhaltGeben());
    }

    /**
     * Test of loeschen method, of class Liste.
     */
    @Test
    public void testLoeschen() {
        Liste<String> instance = new Liste<>();

        instance.einfuegen("a");
        instance.einfuegen("b");
        instance.einfuegen("c");
        instance.positionSetzen(1);
        assertEquals("b",instance.inhaltGeben());
        instance.loeschen();
        assertEquals("c",instance.inhaltGeben());
        instance.loeschen();
        assertEquals("a",instance.inhaltGeben());
        instance.loeschen();
        
    }

    /**
     * Test of positionGeben method, of class Liste.
     */
    @Test
    public void testPositionSetzenGeben() {
        Liste<String> instance = new Liste<>();

        assertEquals((Integer)0,instance.laengeGeben());
        
        instance.einfuegen("a");
        assertEquals((Integer)0,instance.positionGeben());
        assertEquals((Integer)1,instance.laengeGeben());
        instance.einfuegen("b");
        assertEquals((Integer)1,instance.positionGeben());
        assertEquals((Integer)2,instance.laengeGeben());
        instance.einfuegen("c");
        assertEquals((Integer)2,instance.positionGeben());
        assertEquals((Integer)3,instance.laengeGeben());
        
        instance.positionSetzen(1);
        assertEquals("b",instance.inhaltGeben());
        assertEquals((Integer)1,instance.positionGeben());
        instance.loeschen();
        assertEquals((Integer)1,instance.positionGeben());
    }
  
}
